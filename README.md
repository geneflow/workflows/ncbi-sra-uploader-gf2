# NCBI SRA batch upload tool

Uploading to the NCBI site at times can be very cumbersome. To simpilify this process the SCBS team has developed a batch upload tool to simplify uploading multiple files individually, avoid loss of connection. The process is as follows:
1. [Create/login to SciComp](#scicomp)
2. [Create/login to NCBI](#ncbi)
3. [Retrieve ftp credentials from NCBI](#credentials)
4. [Install SRA batch upload tool GeneFlow workflow](#install)
5. [Upload files](#upload)
6. [Return to the NCBI and select your preload folder. This folder can be loaded before or after a submission has started.](#select)

#### Current file types supported:
- .fastq, .fastq.gz, .fq, .fq.gz, .bam, .fast5, bas.h5, .bax.h5, hdf5

# 1. Create a scicomp account <a name="scicomp"></a>
The first step in this process is to create a Scicomp account. This can be done by going to the OAMD SCBS website found at [info.biotech.cdc.gov](https://info.biotech.cdc.gov/info/) 

<img src="./images/new_infosite.png" width=700></img>



Fill out the account request by entering your name and email address. Then select the account type needed.

Some jobs may take a while to complete so it's suggested to submit them to the HPC nodes on Biolinux or Apsen. For this reason, we suggest that you request access to the **HPC** account. However, this is not required. At a minimum an **SCBS** account is needed. 

<img src="./images/scicomp_account_new.png" width=700></img>

# 2. NCBI account creation <a name="ncbi"></a>
If you don't currently have an NCBI account you will have to create one by going to [NCBI Account Registration](https://www.ncbi.nlm.nih.gov/account/register/). We suggest that you create an account for your group. To do so you will need a new email from the CDC ITSO team. 

<img src="./images/NCBI_account_creation.png" width=700></img>
Once you have an NCBI account and are logged in, you can preload a folder using the ftp instructions. 

<img src="./images/select_ftp.png" width=700></img>

# 3. Retrieve ftp credentials from NCBI <a name="credentials"></a>
From the FTP instructions you will need your account folder (2). This is typically your email address followed by a random string. Do **not** include "uploads/". So in the example below you would need to use "qdy1_cdc.gov_XXXXXX". You do not have to name your folder "new_folder", we suggest that you give a more descriptive name in case you have multiple projects to upload in the same time frame.

<img src="./images/ncbi_upload_details.png" width=700></img>

# 4. Install SRA batch upload tool GeneFlow workflow <a name="install"></a>
Once you have the FTP creditials you can login to biolinux (biolinux2020 or aspen) to install and run the SRA batch upload geneflow workflow. We suggest you submit to the HPC queue or use screen to run the process in the background. There are several installaltion options, the instructions are as follows:

This is the universal install, and should work on any environment where conda is installed:
```
cd ~  # make sure you're in your home directory

git clone https://gitlab.com/geneflow/workflows/ncbi-sra-uploader-gf2.git
cd ncbi-sra-uploader-gf2

make install  # this will install all dependencies

source ~/.bashrc
conda activate ~/ncbi-sra-uploader-gf2/ncbi-sra-uploader-env
```
It is always a good idea to deactivate your enviorment after you are done running the programs. This can be done by typing `conda deactivate`.

This conda install is currently available in the scicomp environment and can be activated with the following commands:

```
module load ncbi-sra-uploader
conda activate /apps/standalone/conda/ncbi-sra-uploader-env
gf run -g ncbi-sra-uploader/0.4
```

The following instructions can be run from inside the scicomp environment, but will not allow you to use the grapical interface. Once installed, you will on have to run the run workflow command for any future uploads.

```
module load geneflow/latest #load geneflow module

#install workflow
gf install-workflow -g https://gitlab.com/geneflow/workflows/ncbi-sra-uploader-gf2.git -c --make-apps ./ncbi_sra_ge_tool -f 

#run workflow
gf run ./ncbi_sra_ge_tool/ -o ./output -n upload-job --in.reads=ncbi-ftp-tool-gf/test/data --param.output_dir=new_folder --param.remote=XXXXXXXXX --ec default:local  --ep default.slots:1 --em default:environment
```

# 5. Upload load you files <a name="upload"></a>
To run the workflow from the command line, use the following command, be sure to change:
- --in.reads=
- --param.remote=
- --param.new_output_dir= (optional)

```
gf run . -o ./output -n upload-job --in.reads=path/to/your/files --param.new_output_dir=new_folder_on_ncbi --param.remote=qdy1_cdc.gov_XXXXXX
```

To run the workflow using a graphical interface, use the following command from a terminal with in a graphical environment:

```
gf run -g .
```
After several seconds the program will launch (Note: you must use program such as mobaXterm or guacamole for the grachical interface)

<img src="./images/upload_gui.png" width=700></img>

# 6. Select preloaded folder and finish submission <a name="select"></a>
Once the upload has completed, you can return to the NCBI site and select your preload folder. This folder can be loaded before or after a submission has started.

<img src="./images/preloaded_folder.png" width=700></img>

### All done
