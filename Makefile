install: install-miniconda install-conda-env install-geneflow install-workflow

install-miniconda:
	wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
	sh ./Miniconda3-latest-Linux-x86_64.sh -b -u
	PATH=~/miniconda3/bin:${PATH} conda init bash
	PATH=~/miniconda3/bin:${PATH} conda config --set auto_activate_base false
	rm ./Miniconda3-latest-Linux-x86_64.sh

install-conda-env:
	source ~/.bashrc && conda create -y -p ./ncbi-sra-uploader-env -c conda-forge python=3 lftp gooey openssl=1.0

install-geneflow:
	rm -rf ./geneflow2 && git clone https://github.com/CDCgov/geneflow2
	source ~/.bashrc && conda activate ./ncbi-sra-uploader-env && pip3 install ./geneflow2

install-workflow:
	source ~/.bashrc && conda activate ./ncbi-sra-uploader-env && gf install-workflow --make-apps -c .
